FROM python:3.10-bullseye

RUN pip install poetry==1.3.1

WORKDIR /app
COPY poetry.lock pyproject.toml /app

RUN poetry config virtualenvs.create false && poetry install --no-interaction --no-ansi

COPY ./ /app

CMD [ "python", "-m", "prolobot" ]
