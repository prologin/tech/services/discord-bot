# Prolobot

[![discord.py](https://img.shields.io/badge/discord-py-blue.svg)](https://github.com/Rapptz/discord.py)
[![Black coding style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)
[![Python version](https://img.shields.io/badge/python-3.9-blue)](https://www.python.org/downloads/)

Discord bot for the public Prologin Discord server.

## Features

- Require new members to write a form about themselves, which is then validated by moderators
  before allowing access to the server
- Bulk role assignment and direct messages

## Installation

Set up a virtual environment in Python 3.9 or above.

Install the requirements with `pip3 install -r requirements.txt`

Run the bot with `python3 -m prolobot`. You can exit using Ctrl+C or SIGTERM.

### Environment variables

Before running, you must have the following environment variables setup:

- `PROLOBOT_TOKEN`: The Discord bot token
- `PROLOBOT_GUILD_ID`: The ID of the server the bot will run in
- `PROLOBOT_GATING_CHANNEL_ID`: The ID of the channel where gating messages are sent (new members)
- `PROLOBOT_MEMBER_ROLE_ID`: The ID of the member role, assigned to validated new members
- `PROLOBOT_GCC_ROLE_ID`: The ID of the Girls Can Code role, assigned with the GCC button
- `PROLOBOT_ORG_ROLE_ID`: The ID of the role allowed to assign roles to new members
- `PROLOBOT_ORG_COMMANDS_ROLE_ID`: The ID of the role allowed to use the commands in the
  org_commands package

The bot must have the message content and the server members intents enabled.
