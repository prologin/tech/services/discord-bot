import os
import sys
import time
import signal
import asyncio
import logging
import discord
import argparse

from prolobot.loggers import init_logger
from prolobot.core.bot import Prolobot

log = logging.getLogger("prolobot")


def parse_cli_flags(arguments: str):
    parser = argparse.ArgumentParser(
        prog="Prologin Discord bot", description="Discord bot for the Prologin server"
    )
    parser.add_argument("--debug", action="store_true", help="Enable debug logs")
    parser.add_argument("--token", action="store", type=str, help="Set the bot's token")
    args = parser.parse_args(arguments)
    return args


def print_welcome():
    print("{0:-^50}".format(" Prologin Discord bot "))
    print("")
    print(" {0:<20} {1:>10}".format("Discord.py version:", discord.__version__))
    print("")


async def shutdown_handler(bot: Prolobot, signal_type: str = None):
    if signal_type:
        log.info(f"Received {signal_type}, stopping the bot...")
    else:
        log.info("Shutting down the bot...")
    try:
        await bot.close()
    finally:
        pending = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
        [task.cancel() for task in pending]
        await asyncio.gather(*pending, return_exceptions=True)


def main():
    bot = None
    cli_flags = parse_cli_flags(sys.argv[1:])

    print_welcome()

    try:
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        init_logger(cli_flags.debug)

        token = cli_flags.token or os.environ.get("PROLOBOT_TOKEN", None)
        if not token:
            log.error("Token non trouvé!")
            print("Vous devez fournir un token avec le flag --token.")
            time.sleep(1)
            sys.exit(0)

        bot = Prolobot(command_prefix="!")

        try:
            loop.add_signal_handler(
                signal.SIGTERM,
                lambda: asyncio.create_task(shutdown_handler(bot, signal.SIGTERM)),
                bot,
                "SIGTERM",
            )
        except NotImplementedError:
            # Not a UNIX environment (Windows)
            pass

        log.info("Initialized bot, connecting to Discord...")
        loop.run_until_complete(bot.start(token))
    except KeyboardInterrupt:
        if bot is not None:
            loop.run_until_complete(shutdown_handler(bot, "Ctrl+C"))
    except SystemExit:
        if bot is not None:
            loop.run_until_complete(shutdown_handler(bot))
    except Exception:
        log.critical("Unhandled exception.", exc_info=True)
        if bot is not None:
            loop.run_until_complete(shutdown_handler(bot))
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        asyncio.set_event_loop(None)
        loop.stop()
        loop.close()
        sys.exit(bot._shutdown)


if __name__ == "__main__":
    main()
