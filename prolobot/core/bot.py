import os
import discord
import asyncio
import logging
import sys

from importlib.machinery import ModuleSpec
from importlib.util import find_spec

from discord.ext import commands

from prolobot.core.commands import Core
from prolobot.core.settings import Roles


log = logging.getLogger("prolobot.core.bot")

PACKAGES = ["gating", "org_commands"]
GUILD_ID = os.environ.get("PROLOBOT_GUILD_ID", None)
GATING_CHANNEL_ID = os.environ.get("PROLOBOT_GATING_CHANNEL_ID", None)


class Prolobot(commands.Bot):
    """
    Prologin Discord bot
    """

    def __init__(self, command_prefix: str, **options):
        intents = discord.Intents(
            guilds=True, members=True, messages=True, reactions=True, message_content=True
        )
        super().__init__(command_prefix, intents=intents, **options)
        self._shutdown = 0

        self.guild: discord.Guild
        self.roles: Roles
        self.gating_channel: discord.TextChannel

    async def load_extension(self, spec: ModuleSpec):
        name = spec.name.split(".")[-1]
        if name in self.extensions:
            raise RuntimeError(f"Package {spec.name.split('.')[-1]} is already loaded.")

        lib = spec.loader.load_module()
        if not hasattr(lib, "setup"):
            del lib
            raise discord.ClientException(f"extension {name} does not have a setup function")

        try:
            if asyncio.iscoroutinefunction(lib.setup):
                await lib.setup(self)
            else:
                lib.setup(self)
        except Exception:
            self._remove_module_references(lib.__name__)
            self._call_module_finalizers(lib, name)
            raise

    async def on_shard_ready(self, shard_id: int):
        log.debug(f"Connected to shard #{shard_id}")

    async def on_ready(self):
        log.info(f"Successfully logged in as {self.user} ({self.user.id})!")
        await self.add_cog(Core(self))
        self.guild = self.get_guild(int(GUILD_ID))
        if not self.guild:
            log.critical(
                f"Guild with ID {GUILD_ID} cannot be found within the bot! Shutting down..."
            )
            await self.close()
            self._shutdown = 1
            sys.exit(self._shutdown)
        log.info(f'The bot is set to run on guild "{self.guild.name}" with ID {self.guild.id}')
        self.roles = Roles(self.guild)
        self.gating_channel = self.guild.get_channel(int(GATING_CHANNEL_ID))
        if not self.gating_channel:
            log.warning(
                f"Gating channel with ID {GATING_CHANNEL_ID} cannot be found in this guild!"
            )
        log.info("Loading packages...")
        loaded_packages = []
        for package in PACKAGES:
            try:
                await self.load_extension(find_spec("prolobot.packages." + package))
            except Exception:
                log.error(f"Failed to load package {package}", exc_info=True)
            else:
                loaded_packages.append(package)
        if loaded_packages:
            log.info(f"Packages loaded: {', '.join(loaded_packages)}")
        else:
            log.info("No package loaded.")
        print("\n    Prolobot is now operational!\n")

    async def on_command_error(
        self, context: commands.Context, exception: commands.errors.CommandError
    ):
        if isinstance(exception, commands.CommandNotFound):
            return
        if isinstance(exception, commands.MissingRequiredArgument):
            await context.send_help(context.command)
            return
        if isinstance(exception, commands.CommandInvokeError):
            await context.send("Error when executing command.")
            log.error(f"Error in text command {context.command.name}", exc_info=exception)
