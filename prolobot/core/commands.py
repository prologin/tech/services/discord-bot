import time

from typing import TYPE_CHECKING
from discord.ext import commands

if TYPE_CHECKING:
    from .bot import Prolobot


class Core(commands.Cog):
    """
    Core commands of Prologin bot
    """

    def __init__(self, bot: "Prolobot"):
        self.bot = bot

    @commands.command()
    async def ping(self, ctx: commands.Context):
        """
        Affiche le temps de réponse du bot.
        """
        t1 = time.time()
        msg = await ctx.send("Pong!")
        t2 = time.time()
        await msg.edit(content="Pong!\nDelay: `{t}ms`".format(t=round((t2 - t1) * 1000)))
