import os
import discord
import logging

log = logging.getLogger("prolobot.core.settings")

MEMBER_ROLE = os.environ.get("PROLOBOT_MEMBER_ROLE_ID", None)
GCC_ROLE = os.environ.get("PROLOBOT_GCC_ROLE_ID", None)
ORG_ROLE = os.environ.get("PROLOBOT_ORG_ROLE_ID", None)


class Roles:
    member: discord.Role
    gcc: discord.Role
    org: discord.Role

    def __init__(self, guild: discord.Guild):
        roles = {
            "member": MEMBER_ROLE,
            "gcc": GCC_ROLE,
            "org": ORG_ROLE,
        }
        for name, role_id in roles.items():
            role = guild.get_role(int(role_id))
            if not role:
                log.warning(f'Role "{name}" with ID {role_id} cannot be found in this guild!')
            else:
                setattr(self, name, role)
