import discord
import logging
import asyncio
import os

from typing import TYPE_CHECKING

from discord.ext import commands

if TYPE_CHECKING:
    from prolobot.core.bot import Prolobot

log = logging.getLogger("prolobot.packages.org_commands")

ALLOWED_ROLE_ID = int(os.environ.get("PROLOBOT_ORG_COMMANDS_ROLE_ID", 0))


class OrgCommands(commands.Cog):
    """
    Commands for mods and organizers.
    """

    def __init__(self, bot: "Prolobot"):
        self.bot = bot

    @commands.command(require_var_positional=True)
    @commands.has_role(ALLOWED_ROLE_ID)
    async def assignroles(
        self, ctx: commands.Context, roles: commands.Greedy[discord.Role], *members: discord.Member
    ):
        """
        Assigne un ou plusieurs rôles à un ou plusieurs membres.

        Donnez la liste de rôles (noms entre guillmets ou IDs) puis la liste de membres concernés \
(noms entre guillmets, mentions ou IDs également).
        Exemple : !assignroles Role1 "Role 2 avec des espaces" "Membre 1" Membre2#0000 ...
        """
        message = await ctx.send(f"Assignation des rôles en cours... (0/{len(members)})")
        done = 0
        failed = 0

        async def update_message():
            await asyncio.sleep(5)
            await message.edit(
                content=f"Assignation des rôles en cours... ({done}/{len(members)}))"
            )

        task = self.bot.loop.create_task(update_message())
        for member in members:
            try:
                await member.add_roles(*roles, reason=f"Requête de {ctx.author}")
            except discord.HTTPException:
                log.error(f"Cannot give roles {roles} to member {member}", exc_info=True)
                failed += 1
            else:
                done += 1
        task.cancel()
        if failed:
            await message.edit(
                content=f"Rôle assignés à {done}/{len(members)} membres ({failed} erreurs)"
            )
        else:
            await message.edit(content=f"Rôle assignés à {done} membres.")

    @commands.command()
    @commands.has_role(ALLOWED_ROLE_ID)
    async def mprole(self, ctx: commands.Context, role: discord.Role, *, content: str):
        """
        Envoie un MP à tous les membres d'un rôle.
        """
        members = role.members
        message = await ctx.send(f"Envoi des messages en cours... (0/{len(members)})")
        done = 0
        failed = 0

        async def update_message():
            await asyncio.sleep(5)
            await message.edit(content=f"Envoi des messages en cours... ({done}/{len(members)}))")

        task = self.bot.loop.create_task(update_message())
        for member in members:
            try:
                await member.send(content)
            except discord.Forbidden:
                failed += 1
            except discord.HTTPException:
                log.warning(f"Can't DM member {member} (requested by {ctx.author}", exc_info=True)
                failed += 1
            else:
                done += 1
        task.cancel()
        if failed:
            await message.edit(
                content=f"Messages envoyés à {done}/{len(members)} membres ({failed} erreurs)"
            )
        else:
            await message.edit(content=f"Messages envoyés à {done} membres.")
