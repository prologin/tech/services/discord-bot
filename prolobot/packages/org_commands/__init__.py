from typing import TYPE_CHECKING
from prolobot.packages.org_commands.commands import OrgCommands

if TYPE_CHECKING:
    from prolobot.core.bot import Prolobot


async def setup(bot: "Prolobot"):
    await bot.add_cog(OrgCommands(bot))
