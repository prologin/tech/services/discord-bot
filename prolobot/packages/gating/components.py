from __future__ import annotations

import discord
import logging
import asyncio

from typing import TYPE_CHECKING

from discord.ui import View, Button, Modal, TextInput

if TYPE_CHECKING:
    from prolobot.core.bot import Prolobot

log = logging.getLogger("prolobot.packages.gating")


class ApprobationView(View):
    def __init__(self, bot: "Prolobot", member: discord.Member):
        self.bot = bot
        super().__init__(timeout=None)
        self.add_item(ApproveButton(bot, member))
        self.add_item(GCCButton(bot, member))
        self.add_item(DenyButton(member))

        # Set to True if a DM was sent, avoiding duplicates for multiple roles
        self.sent_dm = False

        # prevent concurrency issues where users are too fast clicking the button
        self.lock = asyncio.Lock()

    async def on_error(self, error: Exception, item: RoleButton, interaction: discord.Interaction):
        log.error(
            f"Unhandled error in approbation view, invoked by {interaction.user}. {item=}",
            exc_info=error,
        )
        try:
            await interaction.response.send_message("Une erreur est survenue.", ephemeral=True)
        except discord.HTTPException:
            # At this point Discord is probably dying
            pass

    async def interaction_check(self, interaction: discord.Interaction) -> bool:
        if (
            self.bot.roles.org not in interaction.user.roles
            and not interaction.user.guild_permissions.administrator
        ):
            await interaction.response.send_message(
                "Vous n'êtes pas autorisé à faire cela !", ephemeral=True
            )
            return False
        else:
            return True


class RoleButton(Button[ApprobationView]):
    def __init__(self, bot: "Prolobot", role: discord.Role, member: discord.Member, **kwargs):
        self.bot = bot
        self.role = role

        # to keep the member object synced, we need to call guild.get_member(ID)
        self.member_id = member.id

        super().__init__(**kwargs)

    async def member_left(self, interaction: discord.Interaction):
        self.view.stop()
        for child in self.view.children:
            child.disabled = True
        embed = interaction.message.embeds[0]
        embed.color = 0xE62A36
        embed.add_field(name="Statut", value="Le membre a quitté le serveur")
        await interaction.response.edit_message(embed=embed, view=self.view)

    async def callback(self, interaction: discord.Interaction):
        # prevent other clicks from being processed at the same time (would cause duplicated DMs)
        async with self.view.lock:

            member = self.bot.guild.get_member(self.member_id)
            if not member:
                return await self.member_left(interaction)

            if self.role in member.roles:
                await interaction.response.send_message(
                    "Le rôle a déjà été attribué", ephemeral=True
                )
                return

            try:
                await member.add_roles(self.role, reason=f"Validé par {interaction.user}")
            except discord.NotFound:
                await self.member_left(interaction)
            except discord.HTTPException:
                log.error(f"Can't add role to member {member}", exc_info=True)
                await interaction.response.send_message(
                    "Une erreur est survenue lors de l'attribution du rôle."
                )
            else:
                self.disabled = True
                self.view.children[2].disabled = True  # Disable ignore button
                embed = interaction.message.embeds[0]
                embed.color = 0x32984B
                string = f"Rôle {self.role.mention} ajouté par {interaction.user.mention}"
                if embed.fields:
                    embed.set_field_at(
                        0, name="Status", value=embed.fields[0].value + "\n" + string
                    )
                else:
                    embed.add_field(name="Statut", value=string)
                await interaction.response.edit_message(embed=embed, view=self.view)
                log.info(f'Added role "{self.role}" to {member}.')

                # Contact in DM only if the member has no roles already
                if not self.view.sent_dm:
                    try:
                        await member.send(
                            f"Hey {member.name}, tu as maintenant accès au serveur de Prologin !"
                        )
                    except discord.HTTPException:
                        pass  # user may have disabled DMs by default
                    else:
                        self.view.sent_dm = True


class ApproveButton(RoleButton):
    def __init__(self, bot: "Prolobot", member: discord.Member):
        super().__init__(
            bot=bot,
            role=bot.roles.member,
            member=member,
            style=discord.ButtonStyle.success,
            label="Accepter (Concours)",
            emoji="\N{HEAVY CHECK MARK}\N{VARIATION SELECTOR-16}",
            custom_id=f"approval-button-{member.id}",
        )


class GCCButton(RoleButton):
    def __init__(self, bot: "Prolobot", member: discord.Member):
        super().__init__(
            bot=bot,
            role=bot.roles.gcc,
            member=member,
            style=discord.ButtonStyle.primary,
            label="Accepter (GCC)",
            emoji="\N{HEAVY CHECK MARK}\N{VARIATION SELECTOR-16}",
            custom_id=f"gcc-button-{member.id}",
        )


class DenyButton(Button[ApprobationView]):
    def __init__(self, member: discord.Member):
        self.member = member
        super().__init__(
            style=discord.ButtonStyle.danger,
            label="Ignorer",
            emoji="\N{HEAVY MULTIPLICATION X}\N{VARIATION SELECTOR-16}",
            custom_id=f"deny-button-{member.id}",
        )

    async def callback(self, interaction: discord.Interaction):
        for component in self.view.children:
            component.disabled = True
        embed = interaction.message.embeds[0]
        embed.color = 0xE62A36
        embed.add_field(name="Statut", value=f"Ignoré par {interaction.user.mention}")
        await interaction.response.edit_message(embed=embed, view=self.view)
        log.info(f"Ignored member {self.member}")


class PresentationForm(Modal, title="Rejoindre le serveur Prologin"):
    description = TextInput(
        label="Présentation",
        style=discord.TextStyle.paragraph,
        placeholder="Une courte description de toi",
        min_length=20,
        max_length=2000,
    )
    custom_id = "prolobot.gating.presentationmodal"

    def __init__(self, bot: "Prolobot"):
        super().__init__()
        self.bot = bot

    async def send_member_response(self, member: discord.Member):
        embed = discord.Embed()
        embed.set_author(name=member, icon_url=(member.avatar or member.default_avatar).url)
        embed.description = self.description.value
        await self.bot.gating_channel.send(embed=embed, view=ApprobationView(self.bot, member))

    async def on_submit(self, interaction: discord.Interaction):
        try:
            await self.send_member_response(interaction.user)
        except Exception:
            log.error(
                "Failed to send member presentation from "
                f"{interaction.user} ({interaction.user.id})",
                exc_info=True,
            )
            await interaction.response.send_message(
                "Une erreur est survenue, veuillez contacter le staff.", ephemeral=True
            )
        else:
            await interaction.response.send_message(
                "Votre message a été envoyé et va être vérifié sous peu.", ephemeral=True
            )
            log.info(
                f"Message received from {interaction.user} "
                f"({interaction.user.id}): {self.description.value}"
            )


class SubmitModal(Button[PresentationForm]):
    def __init__(self, bot: "Prolobot"):
        self.bot = bot
        super().__init__(
            style=discord.ButtonStyle.success,
            label="Rejoindre le serveur",
            emoji="\N{HEAVY CHECK MARK}\N{VARIATION SELECTOR-16}",
            custom_id="prolobot.gating.joinserver",
        )

    async def callback(self, interaction: discord.Interaction):
        if any(
            [
                x in interaction.user.roles
                for x in (self.bot.roles.member, self.bot.roles.gcc, self.bot.roles.org)
            ]
        ):
            await interaction.response.send_message("Vous êtes déjà approuvé !", ephemeral=True)
            return
        try:
            await interaction.response.send_modal(PresentationForm(self.bot))
        except Exception:
            log.error(
                "Failed to send presentation modal to new member "
                f"{interaction.user} ({interaction.user.id})",
                exc_info=True,
            )
