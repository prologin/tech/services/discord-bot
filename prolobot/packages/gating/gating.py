import discord
import logging

from typing import TYPE_CHECKING, Optional

from discord.ui import View
from discord.ext import commands

from prolobot.packages.gating.components import SubmitModal

if TYPE_CHECKING:
    from prolobot.core.bot import Prolobot

log = logging.getLogger("prolobot.packages.gating")


class Gating(commands.Cog):
    """
    Filter members by asking for a message on join.
    """

    def __init__(self, bot: "Prolobot"):
        self.bot = bot
        self.modal_view = View(timeout=None)
        self.modal_view.add_item(SubmitModal(bot))
        bot.add_view(self.modal_view)

    @commands.command()
    @commands.is_owner()
    async def sendintromessage(
        self, ctx: commands.Context, channel: Optional[discord.TextChannel], *, content: str
    ):
        """
        Envoie un message accompagné du bouton pour rejoindre le serveur.

        Si le channel n'est pas spécifié en premier argument, le message sera envoyé dans le
        channel actuel.
        """
        channel = channel or ctx.channel
        await channel.send(content, view=self.modal_view)
        try:
            await ctx.message.add_reaction("\N{WHITE HEAVY CHECK MARK}")
        except discord.HTTPException:
            await ctx.send("Message envoyé.")
