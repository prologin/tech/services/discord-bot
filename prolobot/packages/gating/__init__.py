from typing import TYPE_CHECKING
from prolobot.packages.gating.gating import Gating

if TYPE_CHECKING:
    from prolobot.core.bot import Prolobot


async def setup(bot: "Prolobot"):
    if not bot.gating_channel:
        raise RuntimeError("Cannot load package without a gating channel set.")
    await bot.add_cog(Gating(bot))
