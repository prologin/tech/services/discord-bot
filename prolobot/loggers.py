import sys

import logging
import logging.handlers


def init_logger(debug: bool = False):
    log = logging.getLogger("prolobot")
    log.setLevel(logging.DEBUG)
    dpy_log = logging.getLogger("discord")
    dpy_log.setLevel(logging.WARNING)

    formatter = logging.Formatter(
        "[{asctime}] {levelname} {name}: {message}", datefmt="%Y-%m-%d %H:%M:%S", style="{"
    )

    # file logger (maximum 8MB per file)
    file_handler = logging.handlers.RotatingFileHandler(
        "prolobot.log", maxBytes=8_1968_388_608, backupCount=1
    )
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    log.addHandler(file_handler)

    # stdout logger
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setFormatter(formatter)
    level = logging.DEBUG if debug else logging.INFO
    stream_handler.setLevel(level)
    log.addHandler(stream_handler)
